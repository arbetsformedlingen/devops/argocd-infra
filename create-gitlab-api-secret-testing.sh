# api, developer
export TOKEN="<insert token here>"
oc apply -n openshift-gitops -f - << EOF
apiVersion: v1
kind: Secret
metadata:
  name: argocd-notifications-secret
  labels:
    app.kubernetes.io/part-of: argocd
stringData:
  argocd-notifications-api-token: $TOKEN
type: Opaque
EOF
